#!/bin/sh
set -e

apt-get update
apt-get install -y curl

#UBUNTU_DISTRIUTION=$(lsb_release -cs)
LATEST_STABLE_VERSION=$(curl -s https://golang.org/dl/ | grep 'Stable versions' -A 3 | grep -oP '\Kgo[0-9]+.[0-9]+.[0-9]+')
curl https://dl.google.com/go/$LATEST_STABLE_VERSION.linux-amd64.tar.gz -o go.tar.gz
tar -xvzf go.tar.gz
rm go.tar.gz
sudo mv go /usr/local/
printf 'export PATH=$PATH:/usr/local/go/bin \n'

